### README
data_mm-3.141592
__________________________________________________
### Installation
Clone the repository data_mm_library from
https://bitbucket.org/jiweshkushal/data_mm_library/src/master/

With the current directory set as the cloned folder, run the command
```
pip install data_mm
```
__________________________________________________

### bigqueries

Module bigqueries contains functions which return commonly used aggregated tables (like retention, activation percentage, etc) as dataframes.

Connection to BigQuery database needs to be established and client needs to be defined.

Most of the functions have paramters start_date and end_date.
The inputs to these should be given in the format 'YYYY-MM-DD'.

```
from data_mm import bigqueries  as bq
bq.Dn_Retention(3,'2019-12-04','2019-12-07',client)
```

The input to the table parameter should be a table name in the database, passed as a string.

```
from data_mm import bigqueries  as bq
bq.Per_DNU_Activity('follow','2019-12-04','2019-12-10',client)
```


#### Function Description

1. dn_retention(client,n,start_date,end_date):
Returns the Dn retention (n is an integer): D1 retention for n=1 and so on
Parameters:
-- n: The number of days after D0 for which retention is to be calculated. (default value= 1)
-- start_date: lower limit of the creation date; to be input as 'YYYY-MM-DD'
(default value= Current date - 7 days)
-- end_date: upper limit of the creation date; to be input as 'YYYY-MM-DD'
(default value= Current date - 2 days)
-- client: client connecting to the bigquery database

2. activity_dn_retention(client,table,n,start_date,end_date)
Returns Dn retention for users who performed a certain activity (say, follow) on D0. The activity must be a table in the database.
Parameters:
-- table: Name of the table in the database which filters for users who have fired that event on D0. Input is the table name, such as, 'game_start'.
-- n: The number of days after D0 for which retention is to be calculated. (default value= 1)
-- start_date: lower limit of the creation date; to be input as 'YYYY-MM-DD'
-- end_date: upper limit of the creation date; to be input as 'YYYY-MM-DD'
-- client: client connecting to the bigquery database

3. dn_retained(client,n,start_date,end_date)
Returns a dataframe with all Dn retained users and their creation date
Parameters:
-- n: The number of days after D0 for which retention is to be calculated. (default value= 1)
-- start_date: lower limit of the creation date; to be input as 'YYYY-MM-DD' (default value= Current date - 7 days)
-- end_date: upper limit of the creation date; to be input as 'YYYY-MM-DD' (default value= Current date - 2 days)
-- client: client connecting to the bigquery database

4.  game_act_perc(client,start_date,end_date,gamewise=True)
Returns the gamewise game activation percentage (game time > 59 from game_end)
Parameters:
-- start_date: lower limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 7 days)
-- end_date: upper limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 1 day)
-- client: client connecting to the bigquery database
-- gamewise: a boolean value which returns the data gamewise when set as true (default value= True)

5. per_dnu_activity(client,table,start_date,end_date)
Percentage of New Users who perform a certain activity on their D0. The activity must be a table in the database.
Parameters:
-- table: Name of the table in the database which filters for users who have fired that event on D0. Input is the table name, such as, 'game_start'.
-- start_date: lower limit of the creation date; to be input as 'YYYY-MM-DD' (default value= Current date - 7 days)
-- end_date: upper limit of the creation date; to be input as 'YYYY-MM-DD' (default value= Current date - 1 day)
-- client: client connecting to the bigquery database


6. per_dau_activity(client,table,start_date,end_date)
Percentage of users who perform a certain activity on the given day. The activity must be a table in the database.
Parameters:
-- table: Name of the table in the database which filters for users who have fired that event on the given day. Input is the table name, such as, 'game_start'.
-- n: The number of days after D0 for which retention is to be calculated. (default value= 1)
-- start_date: lower limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 7 days)
-- end_date: upper limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 1 day)
-- client: client connecting to the bigquery database

7. mm_ratio(client,start_date,end_date,gamewise=True):
Returns the Match-Making Rate (Start to Initiate Ratio) gamewise
Parameters:
-- start_date: lower limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 7 days)
-- end_date: upper limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 1 day)
-- client: client connecting to the bigquery database
-- gamewise: a boolean value which returns the data gamewise when set as true (default value= True)

8. gamewise_totals(client,start_date,end_date,gamewise=True):
Returns the total starts, time spent, players, dau and adoption gamewise
Parameters:
-- start_date: lower limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 7 days)
-- end_date: upper limit of the event date; to be input as 'YYYY-MM-DD' (default value= Current date - 1 day)
-- client: client connecting to the bigquery database
-- gamewise: a boolean value which returns the data gamewise when set as true (default value= True)


______________________________________________________________________________


