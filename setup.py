from distutils.core import setup

setup(
    name='data_mm',
    version='3.141592',
    author='Jiwesh Kushal',
    author_email='xld@canteen.com',
    packages=['data_mm'],
    description='Commonly used Functions and Queries in Data Analysis',
    long_description=open('README.md').read(),
    install_requires=['datetime','pandas','numpy'],
)
