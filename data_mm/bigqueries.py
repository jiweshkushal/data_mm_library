import pandas as pd
import numpy as np
from datetime import datetime as dt, timedelta
date_1 = dt.now().date() + timedelta(days=-1)
date_7 = dt.now().date() + timedelta(days=-7)
date_2 = dt.now().date() + timedelta(days=-2)


def dn_retention(client,n=1,start_date=date_7,end_date=date_2):
    str1 = 'D'+str(n)
    str2 = str1+'_ret'
    end_date_2 = dt(int(end_date.split('-')[0]),int(end_date.split('-')[1]),int(end_date.split('-')[2])).date() + timedelta(days=n)
    qry = '''select cr_date, sum(retained)*100.0/count(retained) as %s
    from (select D0.user_pseudo_id, D0.cr_date, coalesce(one,0) retained from
    (select distinct user_pseudo_id, date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE)) cr_date
    from 
    `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    and date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) = date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE))) D0
    left join
    (select distinct user_pseudo_id, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1, 1 one from 
    `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    ) Dn
    on date_add(D0.cr_date, interval %d day) = Dn.date1
    and D0.user_pseudo_id = Dn.user_pseudo_id)
    group by cr_date
    order by cr_date'''%(str2,start_date,end_date,start_date,end_date_2,n)
    df1 = client.query(qry).result().to_dataframe()
    return df1


def get_data(qry,client):
    df = client.query(qry).result().to_dataframe()
    return df




# In[35]:


def activity_dn_retention(client,table,n=1,start_date=date_7,end_date=date_2):
    str1 = 'D'+str(n)
    str2 = str1+'_ret'
    str_table =  '`hello-play-prod.firebase_events_flat.' + table + '`'
    end_date_2 = datetime(int(end_date.split('-')[0]),int(end_date.split('-')[1]),int(end_date.split('-')[2])).date() + timedelta(days=n)
    qry = '''select cr_date, sum(retained)*100.0/count(retained) as %s
    from (select D0.user_pseudo_id, D0.cr_date, coalesce(one,0) retained from
    (select distinct user_pseudo_id, date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE)) cr_date
    from 
    %s
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    and date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) = date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE))) D0
    left join
    (select distinct user_pseudo_id, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1, 1 one from 
    `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    ) Dn
    on date_add(D0.cr_date, interval %d day) = Dn.date1
    and D0.user_pseudo_id = Dn.user_pseudo_id)
    group by cr_date
    order by cr_date'''%(str2,str_table,start_date,end_date,start_date,end_date_2,n)
    df1 = client.query(qry).result().to_dataframe()
    return df1



# In[41]:


def dn_retained(client,n=1,start_date=date_7,end_date=date_2):
    till_date = datetime(int(end_date.split('-')[0]),int(end_date.split('-')[1]),int(end_date.split('-')[2])).date() + timedelta(days=n)
    qry = '''select D0.user_pseudo_id, D0.cr_date from
    (select distinct user_pseudo_id, date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE)) cr_date from 
    `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    and date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) = date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE))) D0
    inner join
    (select distinct user_pseudo_id, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1 from 
    `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    ) Dn
    on date_add(D0.cr_date, interval %d day) = Dn.date1
    and D0.user_pseudo_id = Dn.user_pseudo_id'''%(start_date,end_date,start_date,till_date,n)
    df = client.query(qry).result().to_dataframe()
    return df


# In[45]:


def game_act_perc(client,start_date=date_7,end_date=date_1,gamewise=True):
    qry = '''select starts_new.date1, starts_new.event_prop_game game,
    new_starts starts, new_activates*100.0/new_starts act_perc from 
    (select count(distinct event_prop_giid) new_starts, event_prop_game, date(event_timestamp_ts) date1
    from  `hello-play-prod.firebase_events_flat.game_start`
    where DATE(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) BETWEEN  '%s' and '%s'
    group by date1, event_prop_game) starts_new
    left join
    (select count(distinct event_prop_giid) new_activates, event_prop_game, date(event_timestamp_ts) date1
    from  `hello-play-prod.firebase_events_flat.game_end`
    where DATE(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) BETWEEN '%s' and '%s'
    and cast(event_prop_game_time as int64) > 59
    group by date1, event_prop_game) activates_new
    on activates_new.date1 = starts_new.date1
    and activates_new.event_prop_game = starts_new.event_prop_game
    order by date1, game'''%(start_date,end_date,start_date,end_date)
    dfGamewise = client.query(qry).result().to_dataframe()
    df2 = dfGamewise.copy()
    df2['activates'] = df2['starts']*df2['act_perc']/100
    df = df2.groupby('date1')[['starts','activates']].sum().reset_index()
    df['act_perc'] = df['activates']*100.0/df['starts']
    df = df.drop(columns={'activates'})
    if(gamewise):
        return dfGamewise
    else:
        return df


def mm_ratio(client,start_date=date_7,end_date=date_1,gamewise=True):
    qry = '''select init_table.date1, init_table.event_prop_game game,
    initiates, starts*100.0/initiates mm_ratio from 
    (select count(distinct event_prop_giid) initiates, event_prop_game, date(event_timestamp_ts) date1
    from  `hello-play-prod.firebase_events_flat.game_initiated`
    where DATE(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) BETWEEN  '%s' and '%s'
    group by date1, event_prop_game) init_table
    left join
    (select count(distinct event_prop_giid) starts, event_prop_game, date(event_timestamp_ts) date1
    from  `hello-play-prod.firebase_events_flat.game_start`
    where DATE(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) BETWEEN '%s' and '%s'
    group by date1, event_prop_game) st_table
    on init_table.date1 = st_table.date1
    and init_table.event_prop_game = st_table.event_prop_game
    order by date1, game'''%(start_date,end_date,start_date,end_date)
    dfGamewise = client.query(qry).result().to_dataframe()
    df2 = dfGamewise.copy()
    df2['starts'] = df2['initiates']*df2['mm_ratio']/100
    df = df2.groupby('date1')[['initiates','starts']].sum().reset_index()
    df['mm_ratio'] = df['starts']*100.0/df['initiates']
    df = df.drop(columns={'starts'})
    if(gamewise):
        return dfGamewise
    else:
        return df


# In[55]:


def per_dnu_activity(client,table,start_date=date_7,end_date=date_1):
    str_table =  '`hello-play-prod.firebase_events_flat.' + table + '`'
    qry = '''select activity_new.date1, activity_newusers*100.0/dnu1 as perc_dnu from 
    (select count(distinct user_pseudo_id) activity_newusers, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1
    from %s
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) = date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE))
    and date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    group by date1
    ) activity_new
    join
    (select count(distinct user_pseudo_id) dnu1, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1
    from `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    and date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) = date(TIMESTAMP_ADD( timestamp_micros(safe_cast(user_first_touch_timestamp as int64)),INTERVAL 330 MINUTE))
    group by date1
    ) dnu
    on activity_new.date1 = dnu.date1
    order by activity_new.date1 desc'''%(str_table,start_date,end_date,start_date,end_date)
    df = client.query(qry).result().to_dataframe()
    return df


# In[57]:


def per_dau_activity(client,table,start_date=date_7,end_date=date_1):
    str_table =  '`hello-play-prod.firebase_events_flat.' + table + '`'
    qry = '''select activity.date1, activity_users*100.0/dnu1 as perc_dau from 
    (select count(distinct user_pseudo_id) activity_users, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1
    from %s
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    group by date1
    ) activity
    join
    (select count(distinct user_pseudo_id) dnu1, date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) date1
    from `hello-play-prod.firebase_events_flat.session_start`
    where date(TIMESTAMP_ADD(event_timestamp_ts,INTERVAL 0 MINUTE)) between '%s' and '%s'
    group by date1
    ) dau
    on activity.date1 = dau.date1
    order by activity.date1 desc'''%(str_table,start_date,end_date,start_date,end_date)
    df = client.query(qry).result().to_dataframe()
    return df


def gamewise_totals(client,start_date=date_7,end_date=date_1,gamewise=True):
    qry = '''select dau_table.date1, game, starts, game_time, players, players_all, dau, players*100.0/players_all as adoption from
    (SELECT count(distinct user_pseudo_id) dau, date(event_timestamp_ts) date1
    FROM `hello-play-prod.firebase_events_flat.session_start`
    where date(event_timestamp_ts) between '%s' and '%s'
    group by date1) dau_table
    join
    (SELECT count(distinct user_pseudo_id) players_all, date(event_timestamp_ts) date1
    FROM `hello-play-prod.firebase_events_flat.game_start`
    where date(event_timestamp_ts) between '%s' and '%s'
    group by date1) player_count
    on player_count.date1 = dau_table.date1
    join
    (select date1, game, count(distinct giid) starts,
    count(distinct user_pseudo_id) players, sum(game_time) game_time from
    (SELECT distinct event_prop_giid giid, user_pseudo_id, date(event_timestamp_ts) date1,
    event_prop_game game, min(cast(event_prop_game_time as int64)) game_time
    FROM `hello-play-prod.firebase_events_flat.game_end`
    WHERE DATE(event_timestamp_ts) between '%s' and '%s'
    group by giid, game, user_pseudo_id, date1)
    group by game, date1
    order by date1, game) stats
    on stats.date1 = dau_table.date1
    order by date1, game'''%(start_date,end_date,start_date,end_date,start_date,end_date)
    dfGamewise = client.query(qry).result().to_dataframe()
    dfgame = dfGamewise[['date1','game','starts','game_time']]
    dfabs = dfGamewise[['date1','players_all','dau']].drop_duplicates()
    dfagg = dfgame.groupby('date1')[['starts','game_time']].sum().reset_index()
    dfagg = dfagg.merge(dfabs)
    if(gamewise):
        return dfGamewise
    else:
        return dfagg




